/*
    Copyright © 2017 Harald Sitter <sitter@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQuick.Layouts 1.0

import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0

import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13

import org.kde.plasma.private.tuxedo 0.1

Item {
    id: main
    ColumnLayout{
        spacing: 2    
        ComboBox {
            model: ["First", "Second", "Third"]
        }
        ComboBox {
            textRole: "key"
            model: ListModel {
                ListElement { key: "moo"; value: 123 }
                ListElement { key: "2nd"; value: 456 }
                ListElement { key: "3rd"; value: 789 }
            }
        }
        RowLayout{
            Label {
                text: "Left 👈🏽"
                font.pixelSize: 22
                font.italic: true
            }
            TextField {
                placeholderText: qsTr("Enter colour")
                onTextChanged: {
                    Daemon.colorLeft = text
                }
            }
        }
        RowLayout{
            Label {
                text: "Centre 👇🏿"
                font.pixelSize: 22
                font.italic: true
            }
            TextField {
                placeholderText: qsTr("Enter colour")
                onTextChanged: {
                    Daemon.colorCenter = text
                }
            }
        }
        RowLayout{
            Label {
                text: "Right 👉🏼"
                font.pixelSize: 22
                font.italic: true
            }
            TextField {
                placeholderText: qsTr("Enter colour")
                onTextChanged: {
                    Daemon.colorRight = text
                }
            }
        }
    }

    Layout.minimumHeight: units.gridUnit * 12
    Layout.minimumWidth: units.gridUnit * 12
    Layout.preferredHeight: units.gridUnit * 20
    Layout.preferredWidth: units.gridUnit * 20
    //    Plasmoid.switchWidth: units.gridUnit * 12
    //    Plasmoid.switchHeight: units.gridUnit * 12

     // State is set through timer so we avoid state-switch spam (see below).
    Plasmoid.status: PlasmaCore.Types.PassiveStatus
    // FIXME: need to talk to AndreasK about iconing
    Plasmoid.icon: "apper" // plasmoid.status === PlasmaCore.Types.ActiveStatus ? "package-install" : "apper"
    Plasmoid.toolTipMainText: stateText()
    Plasmoid.toolTipSubText: flavorText()
    Plasmoid.compactRepresentation: PlasmaCore.IconItem {
        source: plasmoid.icon
        colorGroup: PlasmaCore.ColorScope.colorGroup
    }
    Component.onCompleted: console.log(Daemon.lastPackage)
}
