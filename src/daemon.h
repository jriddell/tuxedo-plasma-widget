#ifndef DAEMON_H
#define DAEMON_H

#include <QObject>
#include <QFile>
#include <QDebug>
#include "client.h"

class Daemon : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString mode READ mode WRITE setMode NOTIFY modeChanged)
    Q_PROPERTY(int brightness READ brightness WRITE setBrightness NOTIFY brightnessChanged)
    Q_PROPERTY(QString colorLeft READ colorLeft WRITE setColorLeft NOTIFY colorLeftChanged)
    Q_PROPERTY(QString colorRight READ colorRight WRITE setColorRight NOTIFY colorRightChanged)
    Q_PROPERTY(QString colorCenter READ colorCenter WRITE setColorCenter NOTIFY colorCenterChanged)
    Q_PROPERTY(QString colorExtra READ colorExtra WRITE setColorExtra NOTIFY colorExtraChanged)
    Q_PROPERTY(QString state READ state WRITE setState NOTIFY stateChanged)

    
public:
    QString m_mode;
    int m_brightness=-1;
    QString m_colorLeft;
    QString m_colorRight;
    QString m_colorCenter;
    QString m_colorExtra;
    QString m_state;
    Daemon()=default;

    void setMode(const QString &mode) 
    {
    }
    QString mode()
    {
        return m_mode;
    }
    void setBrightness(const int &brightness) 
    {
    }
    int brightness()
    {
        return m_brightness;
    }
    void setColorLeft(const QString &colorLeft) 
    {
        OrgKdePlasmaTuxedoInterface interface("org.kde.plasma.tuxedo", "/", QDBusConnection::systemBus());
        interface.setColorLeft(QString::number(colorLeft.toInt(nullptr, 16)));
    }
    QString colorLeft()
    {
        return m_colorLeft;
    }
    void setColorRight(const QString &colorRight) 
    {
        OrgKdePlasmaTuxedoInterface interface("org.kde.plasma.tuxedo", "/", QDBusConnection::systemBus());
        interface.setColorRight(QString::number(colorRight.toInt(nullptr, 16)));
        qDebug() << "XXX colorRight" << colorRight;
    }
    QString colorRight()
    {
        return m_colorRight;
    }
    void setColorCenter(const QString &colorCenter) 
    {
        OrgKdePlasmaTuxedoInterface interface("org.kde.plasma.tuxedo", "/", QDBusConnection::systemBus());
        interface.setColorCenter(QString::number(colorCenter.toInt(nullptr, 16)));
    }
    QString colorCenter()
    {
        return m_colorCenter;
    }
    void setColorExtra(const QString &colorExtra) 
    {
    }
    QString colorExtra()
    {
        return m_colorExtra;
    }
    void setState(const QString &state) 
    {
    }
    QString state()
    {
        return m_state;
    }
    
signals:
    
    void modeChanged();
    void brightnessChanged();
    void colorLeftChanged();
    void colorRightChanged();
    void colorCenterChanged();
    void colorExtraChanged();
    void stateChanged();
};

#endif // DAEMON_H
