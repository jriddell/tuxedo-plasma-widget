/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  jr <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUXEDODAEMON_H
#define TUXEDODAEMON_H

#include <QObject>

/**
 * @todo write docs
 */
class TuxedoDaemon : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.plasma.tuxedo")

public:
    /**
     * Default constructor
     */
    TuxedoDaemon();

    /**
     * Destructor
     */
    ~TuxedoDaemon();

public slots:
    void setColorLeft(const QString &colorLeft);
    void setColorRight(const QString &colorRight);
    void setColorCenter(const QString &colorCenter);
};

#endif // TUXEDODAEMON_H
