/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  jr <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tuxedodaemon.h"
#include <QFile>
#include <QDebug>
#include <QCoreApplication>
#include <QDBusConnection>

TuxedoDaemon::TuxedoDaemon()
{
    
}

TuxedoDaemon::~TuxedoDaemon()
{
}

void TuxedoDaemon::setColorLeft(const QString& colorLeft) {    
    QFile file("/sys/devices/platform/tuxedo_keyboard/color_left");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        Q_ASSERT(false);
    }
    qint64 foo = file.write(colorLeft.toLatin1());
    qDebug() << foo << "XXX" << colorLeft;
}

void TuxedoDaemon::setColorRight(const QString& colorRight) {    
    QFile file("/sys/devices/platform/tuxedo_keyboard/color_right");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        Q_ASSERT(false);
    }
    qint64 foo = file.write(colorRight.toLatin1());
    qDebug() << foo << "XXXcolorRight" << colorRight;
}

void TuxedoDaemon::setColorCenter(const QString& colorCenter) {    
    QFile file("/sys/devices/platform/tuxedo_keyboard/color_center");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        Q_ASSERT(false);
    }
    qint64 foo = file.write(colorCenter.toLatin1());
    qDebug() << foo << "XXX" << colorCenter;
}

int main(int argc, char **argv) {
    QCoreApplication app(argc, argv);
    TuxedoDaemon d;
    bool dbusRegisterService = QDBusConnection::systemBus().registerService("org.kde.plasma.tuxedo");
    qDebug() << "dbusRegisterService" << dbusRegisterService;
    bool dbusReturn = QDBusConnection::systemBus().registerObject("/", &d, QDBusConnection::ExportAllSlots);
    qDebug() << "dbusReturn" << dbusReturn;
    return app.exec();
}
